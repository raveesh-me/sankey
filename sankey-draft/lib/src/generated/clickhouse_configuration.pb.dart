//
//  Generated code. Do not modify.
//  source: clickhouse_configuration.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'clickhouse_configuration.pbenum.dart';

export 'clickhouse_configuration.pbenum.dart';

class NameAndType extends $pb.GeneratedMessage {
  factory NameAndType({
    $core.String? name,
    $core.String? type,
  }) {
    final $result = create();
    if (name != null) {
      $result.name = name;
    }
    if (type != null) {
      $result.type = type;
    }
    return $result;
  }
  NameAndType._() : super();
  factory NameAndType.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NameAndType.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'NameAndType', package: const $pb.PackageName(_omitMessageNames ? '' : 'clickhouse.grpc'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'name')
    ..aOS(2, _omitFieldNames ? '' : 'type')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NameAndType clone() => NameAndType()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NameAndType copyWith(void Function(NameAndType) updates) => super.copyWith((message) => updates(message as NameAndType)) as NameAndType;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static NameAndType create() => NameAndType._();
  NameAndType createEmptyInstance() => create();
  static $pb.PbList<NameAndType> createRepeated() => $pb.PbList<NameAndType>();
  @$core.pragma('dart2js:noInline')
  static NameAndType getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NameAndType>(create);
  static NameAndType? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get type => $_getSZ(1);
  @$pb.TagNumber(2)
  set type($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasType() => $_has(1);
  @$pb.TagNumber(2)
  void clearType() => clearField(2);
}

/// Describes an external table - a table which will exists only while a query is executing.
class ExternalTable extends $pb.GeneratedMessage {
  factory ExternalTable({
    $core.String? name,
    $core.Iterable<NameAndType>? columns,
    $core.List<$core.int>? data,
    $core.String? format,
    $core.Map<$core.String, $core.String>? settings,
    $core.String? compressionType,
  }) {
    final $result = create();
    if (name != null) {
      $result.name = name;
    }
    if (columns != null) {
      $result.columns.addAll(columns);
    }
    if (data != null) {
      $result.data = data;
    }
    if (format != null) {
      $result.format = format;
    }
    if (settings != null) {
      $result.settings.addAll(settings);
    }
    if (compressionType != null) {
      $result.compressionType = compressionType;
    }
    return $result;
  }
  ExternalTable._() : super();
  factory ExternalTable.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ExternalTable.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'ExternalTable', package: const $pb.PackageName(_omitMessageNames ? '' : 'clickhouse.grpc'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'name')
    ..pc<NameAndType>(2, _omitFieldNames ? '' : 'columns', $pb.PbFieldType.PM, subBuilder: NameAndType.create)
    ..a<$core.List<$core.int>>(3, _omitFieldNames ? '' : 'data', $pb.PbFieldType.OY)
    ..aOS(4, _omitFieldNames ? '' : 'format')
    ..m<$core.String, $core.String>(5, _omitFieldNames ? '' : 'settings', entryClassName: 'ExternalTable.SettingsEntry', keyFieldType: $pb.PbFieldType.OS, valueFieldType: $pb.PbFieldType.OS, packageName: const $pb.PackageName('clickhouse.grpc'))
    ..aOS(6, _omitFieldNames ? '' : 'compressionType')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ExternalTable clone() => ExternalTable()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ExternalTable copyWith(void Function(ExternalTable) updates) => super.copyWith((message) => updates(message as ExternalTable)) as ExternalTable;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static ExternalTable create() => ExternalTable._();
  ExternalTable createEmptyInstance() => create();
  static $pb.PbList<ExternalTable> createRepeated() => $pb.PbList<ExternalTable>();
  @$core.pragma('dart2js:noInline')
  static ExternalTable getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ExternalTable>(create);
  static ExternalTable? _defaultInstance;

  /// Name of the table. If omitted, "_data" is used.
  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  /// Columns of the table. Types are required, names can be omitted. If the names are omitted, "_1", "_2", ... is used.
  @$pb.TagNumber(2)
  $core.List<NameAndType> get columns => $_getList(1);

  /// Data to insert to the external table.
  /// If a method with streaming input (i.e. ExecuteQueryWithStreamInput() or ExecuteQueryWithStreamIO()) is used,
  /// then data for insertion to the same external table can be split between multiple QueryInfos.
  @$pb.TagNumber(3)
  $core.List<$core.int> get data => $_getN(2);
  @$pb.TagNumber(3)
  set data($core.List<$core.int> v) { $_setBytes(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasData() => $_has(2);
  @$pb.TagNumber(3)
  void clearData() => clearField(3);

  /// Format of the data to insert to the external table.
  @$pb.TagNumber(4)
  $core.String get format => $_getSZ(3);
  @$pb.TagNumber(4)
  set format($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasFormat() => $_has(3);
  @$pb.TagNumber(4)
  void clearFormat() => clearField(4);

  /// Settings for executing that insertion, applied after QueryInfo.settings.
  @$pb.TagNumber(5)
  $core.Map<$core.String, $core.String> get settings => $_getMap(4);

  /// Compression type used to compress `data`.
  /// Supported values: none, gzip(gz), deflate, brotli(br), lzma(xz), zstd(zst), lz4, bz2.
  @$pb.TagNumber(6)
  $core.String get compressionType => $_getSZ(5);
  @$pb.TagNumber(6)
  set compressionType($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasCompressionType() => $_has(5);
  @$pb.TagNumber(6)
  void clearCompressionType() => clearField(6);
}

class ObsoleteTransportCompression extends $pb.GeneratedMessage {
  factory ObsoleteTransportCompression({
    ObsoleteTransportCompression_CompressionAlgorithm? algorithm,
    ObsoleteTransportCompression_CompressionLevel? level,
  }) {
    final $result = create();
    if (algorithm != null) {
      $result.algorithm = algorithm;
    }
    if (level != null) {
      $result.level = level;
    }
    return $result;
  }
  ObsoleteTransportCompression._() : super();
  factory ObsoleteTransportCompression.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ObsoleteTransportCompression.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'ObsoleteTransportCompression', package: const $pb.PackageName(_omitMessageNames ? '' : 'clickhouse.grpc'), createEmptyInstance: create)
    ..e<ObsoleteTransportCompression_CompressionAlgorithm>(1, _omitFieldNames ? '' : 'algorithm', $pb.PbFieldType.OE, defaultOrMaker: ObsoleteTransportCompression_CompressionAlgorithm.NO_COMPRESSION, valueOf: ObsoleteTransportCompression_CompressionAlgorithm.valueOf, enumValues: ObsoleteTransportCompression_CompressionAlgorithm.values)
    ..e<ObsoleteTransportCompression_CompressionLevel>(2, _omitFieldNames ? '' : 'level', $pb.PbFieldType.OE, defaultOrMaker: ObsoleteTransportCompression_CompressionLevel.COMPRESSION_NONE, valueOf: ObsoleteTransportCompression_CompressionLevel.valueOf, enumValues: ObsoleteTransportCompression_CompressionLevel.values)
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ObsoleteTransportCompression clone() => ObsoleteTransportCompression()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ObsoleteTransportCompression copyWith(void Function(ObsoleteTransportCompression) updates) => super.copyWith((message) => updates(message as ObsoleteTransportCompression)) as ObsoleteTransportCompression;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static ObsoleteTransportCompression create() => ObsoleteTransportCompression._();
  ObsoleteTransportCompression createEmptyInstance() => create();
  static $pb.PbList<ObsoleteTransportCompression> createRepeated() => $pb.PbList<ObsoleteTransportCompression>();
  @$core.pragma('dart2js:noInline')
  static ObsoleteTransportCompression getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ObsoleteTransportCompression>(create);
  static ObsoleteTransportCompression? _defaultInstance;

  @$pb.TagNumber(1)
  ObsoleteTransportCompression_CompressionAlgorithm get algorithm => $_getN(0);
  @$pb.TagNumber(1)
  set algorithm(ObsoleteTransportCompression_CompressionAlgorithm v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasAlgorithm() => $_has(0);
  @$pb.TagNumber(1)
  void clearAlgorithm() => clearField(1);

  @$pb.TagNumber(2)
  ObsoleteTransportCompression_CompressionLevel get level => $_getN(1);
  @$pb.TagNumber(2)
  set level(ObsoleteTransportCompression_CompressionLevel v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasLevel() => $_has(1);
  @$pb.TagNumber(2)
  void clearLevel() => clearField(2);
}

/// Information about a query which a client sends to a ClickHouse server.
/// The first QueryInfo can set any of the following fields. Extra QueryInfos only add extra data.
/// In extra QueryInfos only `input_data`, `external_tables`, `next_query_info` and `cancel` fields can be set.
class QueryInfo extends $pb.GeneratedMessage {
  factory QueryInfo({
    $core.String? query,
    $core.String? queryId,
    $core.Map<$core.String, $core.String>? settings,
    $core.String? database,
    $core.List<$core.int>? inputData,
    $core.List<$core.int>? inputDataDelimiter,
    $core.String? outputFormat,
    $core.Iterable<ExternalTable>? externalTables,
    $core.String? userName,
    $core.String? password,
    $core.String? quota,
    $core.String? sessionId,
    $core.bool? sessionCheck,
    $core.int? sessionTimeout,
    $core.bool? cancel,
    $core.bool? nextQueryInfo,
    ObsoleteTransportCompression? obsoleteResultCompression,
    $core.String? obsoleteCompressionType,
    $core.int? outputCompressionLevel,
    $core.String? inputCompressionType,
    $core.String? outputCompressionType,
    $core.String? transportCompressionType,
    $core.int? transportCompressionLevel,
    $core.bool? sendOutputColumns,
  }) {
    final $result = create();
    if (query != null) {
      $result.query = query;
    }
    if (queryId != null) {
      $result.queryId = queryId;
    }
    if (settings != null) {
      $result.settings.addAll(settings);
    }
    if (database != null) {
      $result.database = database;
    }
    if (inputData != null) {
      $result.inputData = inputData;
    }
    if (inputDataDelimiter != null) {
      $result.inputDataDelimiter = inputDataDelimiter;
    }
    if (outputFormat != null) {
      $result.outputFormat = outputFormat;
    }
    if (externalTables != null) {
      $result.externalTables.addAll(externalTables);
    }
    if (userName != null) {
      $result.userName = userName;
    }
    if (password != null) {
      $result.password = password;
    }
    if (quota != null) {
      $result.quota = quota;
    }
    if (sessionId != null) {
      $result.sessionId = sessionId;
    }
    if (sessionCheck != null) {
      $result.sessionCheck = sessionCheck;
    }
    if (sessionTimeout != null) {
      $result.sessionTimeout = sessionTimeout;
    }
    if (cancel != null) {
      $result.cancel = cancel;
    }
    if (nextQueryInfo != null) {
      $result.nextQueryInfo = nextQueryInfo;
    }
    if (obsoleteResultCompression != null) {
      $result.obsoleteResultCompression = obsoleteResultCompression;
    }
    if (obsoleteCompressionType != null) {
      $result.obsoleteCompressionType = obsoleteCompressionType;
    }
    if (outputCompressionLevel != null) {
      $result.outputCompressionLevel = outputCompressionLevel;
    }
    if (inputCompressionType != null) {
      $result.inputCompressionType = inputCompressionType;
    }
    if (outputCompressionType != null) {
      $result.outputCompressionType = outputCompressionType;
    }
    if (transportCompressionType != null) {
      $result.transportCompressionType = transportCompressionType;
    }
    if (transportCompressionLevel != null) {
      $result.transportCompressionLevel = transportCompressionLevel;
    }
    if (sendOutputColumns != null) {
      $result.sendOutputColumns = sendOutputColumns;
    }
    return $result;
  }
  QueryInfo._() : super();
  factory QueryInfo.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QueryInfo.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'QueryInfo', package: const $pb.PackageName(_omitMessageNames ? '' : 'clickhouse.grpc'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'query')
    ..aOS(2, _omitFieldNames ? '' : 'queryId')
    ..m<$core.String, $core.String>(3, _omitFieldNames ? '' : 'settings', entryClassName: 'QueryInfo.SettingsEntry', keyFieldType: $pb.PbFieldType.OS, valueFieldType: $pb.PbFieldType.OS, packageName: const $pb.PackageName('clickhouse.grpc'))
    ..aOS(4, _omitFieldNames ? '' : 'database')
    ..a<$core.List<$core.int>>(5, _omitFieldNames ? '' : 'inputData', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(6, _omitFieldNames ? '' : 'inputDataDelimiter', $pb.PbFieldType.OY)
    ..aOS(7, _omitFieldNames ? '' : 'outputFormat')
    ..pc<ExternalTable>(8, _omitFieldNames ? '' : 'externalTables', $pb.PbFieldType.PM, subBuilder: ExternalTable.create)
    ..aOS(9, _omitFieldNames ? '' : 'userName')
    ..aOS(10, _omitFieldNames ? '' : 'password')
    ..aOS(11, _omitFieldNames ? '' : 'quota')
    ..aOS(12, _omitFieldNames ? '' : 'sessionId')
    ..aOB(13, _omitFieldNames ? '' : 'sessionCheck')
    ..a<$core.int>(14, _omitFieldNames ? '' : 'sessionTimeout', $pb.PbFieldType.OU3)
    ..aOB(15, _omitFieldNames ? '' : 'cancel')
    ..aOB(16, _omitFieldNames ? '' : 'nextQueryInfo')
    ..aOM<ObsoleteTransportCompression>(17, _omitFieldNames ? '' : 'obsoleteResultCompression', subBuilder: ObsoleteTransportCompression.create)
    ..aOS(18, _omitFieldNames ? '' : 'obsoleteCompressionType')
    ..a<$core.int>(19, _omitFieldNames ? '' : 'outputCompressionLevel', $pb.PbFieldType.O3)
    ..aOS(20, _omitFieldNames ? '' : 'inputCompressionType')
    ..aOS(21, _omitFieldNames ? '' : 'outputCompressionType')
    ..aOS(22, _omitFieldNames ? '' : 'transportCompressionType')
    ..a<$core.int>(23, _omitFieldNames ? '' : 'transportCompressionLevel', $pb.PbFieldType.O3)
    ..aOB(24, _omitFieldNames ? '' : 'sendOutputColumns')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QueryInfo clone() => QueryInfo()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QueryInfo copyWith(void Function(QueryInfo) updates) => super.copyWith((message) => updates(message as QueryInfo)) as QueryInfo;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static QueryInfo create() => QueryInfo._();
  QueryInfo createEmptyInstance() => create();
  static $pb.PbList<QueryInfo> createRepeated() => $pb.PbList<QueryInfo>();
  @$core.pragma('dart2js:noInline')
  static QueryInfo getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QueryInfo>(create);
  static QueryInfo? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get query => $_getSZ(0);
  @$pb.TagNumber(1)
  set query($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuery() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuery() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get queryId => $_getSZ(1);
  @$pb.TagNumber(2)
  set queryId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasQueryId() => $_has(1);
  @$pb.TagNumber(2)
  void clearQueryId() => clearField(2);

  @$pb.TagNumber(3)
  $core.Map<$core.String, $core.String> get settings => $_getMap(2);

  /// Default database.
  @$pb.TagNumber(4)
  $core.String get database => $_getSZ(3);
  @$pb.TagNumber(4)
  set database($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDatabase() => $_has(3);
  @$pb.TagNumber(4)
  void clearDatabase() => clearField(4);

  /// Input data, used both as data for INSERT query and as data for the input() function.
  @$pb.TagNumber(5)
  $core.List<$core.int> get inputData => $_getN(4);
  @$pb.TagNumber(5)
  set inputData($core.List<$core.int> v) { $_setBytes(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasInputData() => $_has(4);
  @$pb.TagNumber(5)
  void clearInputData() => clearField(5);

  /// Delimiter for input_data, inserted between input_data from adjacent QueryInfos.
  @$pb.TagNumber(6)
  $core.List<$core.int> get inputDataDelimiter => $_getN(5);
  @$pb.TagNumber(6)
  set inputDataDelimiter($core.List<$core.int> v) { $_setBytes(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasInputDataDelimiter() => $_has(5);
  @$pb.TagNumber(6)
  void clearInputDataDelimiter() => clearField(6);

  /// Default output format. If not specified, 'TabSeparated' is used.
  @$pb.TagNumber(7)
  $core.String get outputFormat => $_getSZ(6);
  @$pb.TagNumber(7)
  set outputFormat($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasOutputFormat() => $_has(6);
  @$pb.TagNumber(7)
  void clearOutputFormat() => clearField(7);

  @$pb.TagNumber(8)
  $core.List<ExternalTable> get externalTables => $_getList(7);

  @$pb.TagNumber(9)
  $core.String get userName => $_getSZ(8);
  @$pb.TagNumber(9)
  set userName($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasUserName() => $_has(8);
  @$pb.TagNumber(9)
  void clearUserName() => clearField(9);

  @$pb.TagNumber(10)
  $core.String get password => $_getSZ(9);
  @$pb.TagNumber(10)
  set password($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasPassword() => $_has(9);
  @$pb.TagNumber(10)
  void clearPassword() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get quota => $_getSZ(10);
  @$pb.TagNumber(11)
  set quota($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasQuota() => $_has(10);
  @$pb.TagNumber(11)
  void clearQuota() => clearField(11);

  /// Works exactly like sessions in the HTTP protocol.
  @$pb.TagNumber(12)
  $core.String get sessionId => $_getSZ(11);
  @$pb.TagNumber(12)
  set sessionId($core.String v) { $_setString(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasSessionId() => $_has(11);
  @$pb.TagNumber(12)
  void clearSessionId() => clearField(12);

  @$pb.TagNumber(13)
  $core.bool get sessionCheck => $_getBF(12);
  @$pb.TagNumber(13)
  set sessionCheck($core.bool v) { $_setBool(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasSessionCheck() => $_has(12);
  @$pb.TagNumber(13)
  void clearSessionCheck() => clearField(13);

  @$pb.TagNumber(14)
  $core.int get sessionTimeout => $_getIZ(13);
  @$pb.TagNumber(14)
  set sessionTimeout($core.int v) { $_setUnsignedInt32(13, v); }
  @$pb.TagNumber(14)
  $core.bool hasSessionTimeout() => $_has(13);
  @$pb.TagNumber(14)
  void clearSessionTimeout() => clearField(14);

  /// Set `cancel` to true to stop executing the query.
  @$pb.TagNumber(15)
  $core.bool get cancel => $_getBF(14);
  @$pb.TagNumber(15)
  set cancel($core.bool v) { $_setBool(14, v); }
  @$pb.TagNumber(15)
  $core.bool hasCancel() => $_has(14);
  @$pb.TagNumber(15)
  void clearCancel() => clearField(15);

  /// If true there will be at least one more QueryInfo in the input stream.
  /// `next_query_info` is allowed to be set only if a method with streaming input (i.e. ExecuteQueryWithStreamInput() or ExecuteQueryWithStreamIO()) is used.
  @$pb.TagNumber(16)
  $core.bool get nextQueryInfo => $_getBF(15);
  @$pb.TagNumber(16)
  set nextQueryInfo($core.bool v) { $_setBool(15, v); }
  @$pb.TagNumber(16)
  $core.bool hasNextQueryInfo() => $_has(15);
  @$pb.TagNumber(16)
  void clearNextQueryInfo() => clearField(16);

  /// / Obsolete fields, should not be used in new code.
  @$pb.TagNumber(17)
  ObsoleteTransportCompression get obsoleteResultCompression => $_getN(16);
  @$pb.TagNumber(17)
  set obsoleteResultCompression(ObsoleteTransportCompression v) { setField(17, v); }
  @$pb.TagNumber(17)
  $core.bool hasObsoleteResultCompression() => $_has(16);
  @$pb.TagNumber(17)
  void clearObsoleteResultCompression() => clearField(17);
  @$pb.TagNumber(17)
  ObsoleteTransportCompression ensureObsoleteResultCompression() => $_ensure(16);

  @$pb.TagNumber(18)
  $core.String get obsoleteCompressionType => $_getSZ(17);
  @$pb.TagNumber(18)
  set obsoleteCompressionType($core.String v) { $_setString(17, v); }
  @$pb.TagNumber(18)
  $core.bool hasObsoleteCompressionType() => $_has(17);
  @$pb.TagNumber(18)
  void clearObsoleteCompressionType() => clearField(18);

  /// Compression level.
  /// WARNING: If it's not specified the compression level is set to zero by default which might be not the best choice for some compression types (see below).
  /// The compression level should be in the following range (the higher the number, the better the compression):
  /// none: compression level isn't used
  /// gzip: 0..9; 0 means no compression, 6 is recommended by default (compression level -1 also means 6)
  /// brotli: 0..11
  /// lzma: 0..9; 6 is recommended by default
  /// zstd: 1..22; 3 is recommended by default (compression level 0 also means 3)
  /// lz4: 0..16; values < 0 mean fast acceleration
  /// bz2: 1..9
  @$pb.TagNumber(19)
  $core.int get outputCompressionLevel => $_getIZ(18);
  @$pb.TagNumber(19)
  set outputCompressionLevel($core.int v) { $_setSignedInt32(18, v); }
  @$pb.TagNumber(19)
  $core.bool hasOutputCompressionLevel() => $_has(18);
  @$pb.TagNumber(19)
  void clearOutputCompressionLevel() => clearField(19);

  /// Compression type for `input_data`.
  /// Supported compression types: none, gzip(gz), deflate, brotli(br), lzma(xz), zstd(zst), lz4, bz2.
  /// The client is responsible to compress data before putting it into `input_data`.
  @$pb.TagNumber(20)
  $core.String get inputCompressionType => $_getSZ(19);
  @$pb.TagNumber(20)
  set inputCompressionType($core.String v) { $_setString(19, v); }
  @$pb.TagNumber(20)
  $core.bool hasInputCompressionType() => $_has(19);
  @$pb.TagNumber(20)
  void clearInputCompressionType() => clearField(20);

  /// Compression type for `output_data`, `totals` and `extremes`.
  /// Supported compression types: none, gzip(gz), deflate, brotli(br), lzma(xz), zstd(zst), lz4, bz2.
  /// The client receives compressed data and should decompress it by itself.
  /// Consider also setting `output_compression_level`.
  @$pb.TagNumber(21)
  $core.String get outputCompressionType => $_getSZ(20);
  @$pb.TagNumber(21)
  set outputCompressionType($core.String v) { $_setString(20, v); }
  @$pb.TagNumber(21)
  $core.bool hasOutputCompressionType() => $_has(20);
  @$pb.TagNumber(21)
  void clearOutputCompressionType() => clearField(21);

  /// Transport compression is an alternative way to make the server to compress its response.
  /// This kind of compression implies that instead of compressing just `output` the server will compress whole packed messages of the `Result` type,
  /// and then gRPC implementation on client side will decompress those messages so client code won't be bothered with decompression.
  /// Here is a big difference between the transport compression and the compression enabled by setting `output_compression_type` because
  /// in case of the transport compression the client code receives already decompressed data in `output`.
  /// If the transport compression is not set here it can still be enabled by the server configuration.
  /// Supported compression types: none, deflate, gzip, stream_gzip
  /// Supported compression levels: 0..3
  /// WARNING: Don't set `transport_compression` and `output_compression` at the same time because it will make the server to compress its output twice!
  @$pb.TagNumber(22)
  $core.String get transportCompressionType => $_getSZ(21);
  @$pb.TagNumber(22)
  set transportCompressionType($core.String v) { $_setString(21, v); }
  @$pb.TagNumber(22)
  $core.bool hasTransportCompressionType() => $_has(21);
  @$pb.TagNumber(22)
  void clearTransportCompressionType() => clearField(22);

  @$pb.TagNumber(23)
  $core.int get transportCompressionLevel => $_getIZ(22);
  @$pb.TagNumber(23)
  set transportCompressionLevel($core.int v) { $_setSignedInt32(22, v); }
  @$pb.TagNumber(23)
  $core.bool hasTransportCompressionLevel() => $_has(22);
  @$pb.TagNumber(23)
  void clearTransportCompressionLevel() => clearField(23);

  /// Set it if you want the names and the types of output columns to be sent to the client.
  @$pb.TagNumber(24)
  $core.bool get sendOutputColumns => $_getBF(23);
  @$pb.TagNumber(24)
  set sendOutputColumns($core.bool v) { $_setBool(23, v); }
  @$pb.TagNumber(24)
  $core.bool hasSendOutputColumns() => $_has(23);
  @$pb.TagNumber(24)
  void clearSendOutputColumns() => clearField(24);
}

class LogEntry extends $pb.GeneratedMessage {
  factory LogEntry({
    $core.int? time,
    $core.int? timeMicroseconds,
    $fixnum.Int64? threadId,
    $core.String? queryId,
    LogsLevel? level,
    $core.String? source,
    $core.String? text,
  }) {
    final $result = create();
    if (time != null) {
      $result.time = time;
    }
    if (timeMicroseconds != null) {
      $result.timeMicroseconds = timeMicroseconds;
    }
    if (threadId != null) {
      $result.threadId = threadId;
    }
    if (queryId != null) {
      $result.queryId = queryId;
    }
    if (level != null) {
      $result.level = level;
    }
    if (source != null) {
      $result.source = source;
    }
    if (text != null) {
      $result.text = text;
    }
    return $result;
  }
  LogEntry._() : super();
  factory LogEntry.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LogEntry.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'LogEntry', package: const $pb.PackageName(_omitMessageNames ? '' : 'clickhouse.grpc'), createEmptyInstance: create)
    ..a<$core.int>(1, _omitFieldNames ? '' : 'time', $pb.PbFieldType.OU3)
    ..a<$core.int>(2, _omitFieldNames ? '' : 'timeMicroseconds', $pb.PbFieldType.OU3)
    ..a<$fixnum.Int64>(3, _omitFieldNames ? '' : 'threadId', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..aOS(4, _omitFieldNames ? '' : 'queryId')
    ..e<LogsLevel>(5, _omitFieldNames ? '' : 'level', $pb.PbFieldType.OE, defaultOrMaker: LogsLevel.LOG_NONE, valueOf: LogsLevel.valueOf, enumValues: LogsLevel.values)
    ..aOS(6, _omitFieldNames ? '' : 'source')
    ..aOS(7, _omitFieldNames ? '' : 'text')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LogEntry clone() => LogEntry()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LogEntry copyWith(void Function(LogEntry) updates) => super.copyWith((message) => updates(message as LogEntry)) as LogEntry;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static LogEntry create() => LogEntry._();
  LogEntry createEmptyInstance() => create();
  static $pb.PbList<LogEntry> createRepeated() => $pb.PbList<LogEntry>();
  @$core.pragma('dart2js:noInline')
  static LogEntry getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LogEntry>(create);
  static LogEntry? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get time => $_getIZ(0);
  @$pb.TagNumber(1)
  set time($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTime() => $_has(0);
  @$pb.TagNumber(1)
  void clearTime() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get timeMicroseconds => $_getIZ(1);
  @$pb.TagNumber(2)
  set timeMicroseconds($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTimeMicroseconds() => $_has(1);
  @$pb.TagNumber(2)
  void clearTimeMicroseconds() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get threadId => $_getI64(2);
  @$pb.TagNumber(3)
  set threadId($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasThreadId() => $_has(2);
  @$pb.TagNumber(3)
  void clearThreadId() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get queryId => $_getSZ(3);
  @$pb.TagNumber(4)
  set queryId($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasQueryId() => $_has(3);
  @$pb.TagNumber(4)
  void clearQueryId() => clearField(4);

  @$pb.TagNumber(5)
  LogsLevel get level => $_getN(4);
  @$pb.TagNumber(5)
  set level(LogsLevel v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasLevel() => $_has(4);
  @$pb.TagNumber(5)
  void clearLevel() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get source => $_getSZ(5);
  @$pb.TagNumber(6)
  set source($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasSource() => $_has(5);
  @$pb.TagNumber(6)
  void clearSource() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get text => $_getSZ(6);
  @$pb.TagNumber(7)
  set text($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasText() => $_has(6);
  @$pb.TagNumber(7)
  void clearText() => clearField(7);
}

class Progress extends $pb.GeneratedMessage {
  factory Progress({
    $fixnum.Int64? readRows,
    $fixnum.Int64? readBytes,
    $fixnum.Int64? totalRowsToRead,
    $fixnum.Int64? writtenRows,
    $fixnum.Int64? writtenBytes,
  }) {
    final $result = create();
    if (readRows != null) {
      $result.readRows = readRows;
    }
    if (readBytes != null) {
      $result.readBytes = readBytes;
    }
    if (totalRowsToRead != null) {
      $result.totalRowsToRead = totalRowsToRead;
    }
    if (writtenRows != null) {
      $result.writtenRows = writtenRows;
    }
    if (writtenBytes != null) {
      $result.writtenBytes = writtenBytes;
    }
    return $result;
  }
  Progress._() : super();
  factory Progress.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Progress.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'Progress', package: const $pb.PackageName(_omitMessageNames ? '' : 'clickhouse.grpc'), createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, _omitFieldNames ? '' : 'readRows', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(2, _omitFieldNames ? '' : 'readBytes', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(3, _omitFieldNames ? '' : 'totalRowsToRead', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(4, _omitFieldNames ? '' : 'writtenRows', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(5, _omitFieldNames ? '' : 'writtenBytes', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Progress clone() => Progress()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Progress copyWith(void Function(Progress) updates) => super.copyWith((message) => updates(message as Progress)) as Progress;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static Progress create() => Progress._();
  Progress createEmptyInstance() => create();
  static $pb.PbList<Progress> createRepeated() => $pb.PbList<Progress>();
  @$core.pragma('dart2js:noInline')
  static Progress getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Progress>(create);
  static Progress? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get readRows => $_getI64(0);
  @$pb.TagNumber(1)
  set readRows($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasReadRows() => $_has(0);
  @$pb.TagNumber(1)
  void clearReadRows() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get readBytes => $_getI64(1);
  @$pb.TagNumber(2)
  set readBytes($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasReadBytes() => $_has(1);
  @$pb.TagNumber(2)
  void clearReadBytes() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get totalRowsToRead => $_getI64(2);
  @$pb.TagNumber(3)
  set totalRowsToRead($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTotalRowsToRead() => $_has(2);
  @$pb.TagNumber(3)
  void clearTotalRowsToRead() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get writtenRows => $_getI64(3);
  @$pb.TagNumber(4)
  set writtenRows($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasWrittenRows() => $_has(3);
  @$pb.TagNumber(4)
  void clearWrittenRows() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get writtenBytes => $_getI64(4);
  @$pb.TagNumber(5)
  set writtenBytes($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasWrittenBytes() => $_has(4);
  @$pb.TagNumber(5)
  void clearWrittenBytes() => clearField(5);
}

class Stats extends $pb.GeneratedMessage {
  factory Stats({
    $fixnum.Int64? rows,
    $fixnum.Int64? blocks,
    $fixnum.Int64? allocatedBytes,
    $core.bool? appliedLimit,
    $fixnum.Int64? rowsBeforeLimit,
  }) {
    final $result = create();
    if (rows != null) {
      $result.rows = rows;
    }
    if (blocks != null) {
      $result.blocks = blocks;
    }
    if (allocatedBytes != null) {
      $result.allocatedBytes = allocatedBytes;
    }
    if (appliedLimit != null) {
      $result.appliedLimit = appliedLimit;
    }
    if (rowsBeforeLimit != null) {
      $result.rowsBeforeLimit = rowsBeforeLimit;
    }
    return $result;
  }
  Stats._() : super();
  factory Stats.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Stats.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'Stats', package: const $pb.PackageName(_omitMessageNames ? '' : 'clickhouse.grpc'), createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, _omitFieldNames ? '' : 'rows', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(2, _omitFieldNames ? '' : 'blocks', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(3, _omitFieldNames ? '' : 'allocatedBytes', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..aOB(4, _omitFieldNames ? '' : 'appliedLimit')
    ..a<$fixnum.Int64>(5, _omitFieldNames ? '' : 'rowsBeforeLimit', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Stats clone() => Stats()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Stats copyWith(void Function(Stats) updates) => super.copyWith((message) => updates(message as Stats)) as Stats;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static Stats create() => Stats._();
  Stats createEmptyInstance() => create();
  static $pb.PbList<Stats> createRepeated() => $pb.PbList<Stats>();
  @$core.pragma('dart2js:noInline')
  static Stats getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Stats>(create);
  static Stats? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get rows => $_getI64(0);
  @$pb.TagNumber(1)
  set rows($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRows() => $_has(0);
  @$pb.TagNumber(1)
  void clearRows() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get blocks => $_getI64(1);
  @$pb.TagNumber(2)
  set blocks($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasBlocks() => $_has(1);
  @$pb.TagNumber(2)
  void clearBlocks() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get allocatedBytes => $_getI64(2);
  @$pb.TagNumber(3)
  set allocatedBytes($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAllocatedBytes() => $_has(2);
  @$pb.TagNumber(3)
  void clearAllocatedBytes() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get appliedLimit => $_getBF(3);
  @$pb.TagNumber(4)
  set appliedLimit($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasAppliedLimit() => $_has(3);
  @$pb.TagNumber(4)
  void clearAppliedLimit() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get rowsBeforeLimit => $_getI64(4);
  @$pb.TagNumber(5)
  set rowsBeforeLimit($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasRowsBeforeLimit() => $_has(4);
  @$pb.TagNumber(5)
  void clearRowsBeforeLimit() => clearField(5);
}

class Exception extends $pb.GeneratedMessage {
  factory Exception({
    $core.int? code,
    $core.String? name,
    $core.String? displayText,
    $core.String? stackTrace,
  }) {
    final $result = create();
    if (code != null) {
      $result.code = code;
    }
    if (name != null) {
      $result.name = name;
    }
    if (displayText != null) {
      $result.displayText = displayText;
    }
    if (stackTrace != null) {
      $result.stackTrace = stackTrace;
    }
    return $result;
  }
  Exception._() : super();
  factory Exception.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Exception.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'Exception', package: const $pb.PackageName(_omitMessageNames ? '' : 'clickhouse.grpc'), createEmptyInstance: create)
    ..a<$core.int>(1, _omitFieldNames ? '' : 'code', $pb.PbFieldType.O3)
    ..aOS(2, _omitFieldNames ? '' : 'name')
    ..aOS(3, _omitFieldNames ? '' : 'displayText')
    ..aOS(4, _omitFieldNames ? '' : 'stackTrace')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Exception clone() => Exception()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Exception copyWith(void Function(Exception) updates) => super.copyWith((message) => updates(message as Exception)) as Exception;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static Exception create() => Exception._();
  Exception createEmptyInstance() => create();
  static $pb.PbList<Exception> createRepeated() => $pb.PbList<Exception>();
  @$core.pragma('dart2js:noInline')
  static Exception getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Exception>(create);
  static Exception? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get code => $_getIZ(0);
  @$pb.TagNumber(1)
  set code($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCode() => $_has(0);
  @$pb.TagNumber(1)
  void clearCode() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get displayText => $_getSZ(2);
  @$pb.TagNumber(3)
  set displayText($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDisplayText() => $_has(2);
  @$pb.TagNumber(3)
  void clearDisplayText() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get stackTrace => $_getSZ(3);
  @$pb.TagNumber(4)
  set stackTrace($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasStackTrace() => $_has(3);
  @$pb.TagNumber(4)
  void clearStackTrace() => clearField(4);
}

/// Result of execution of a query which is sent back by the ClickHouse server to the client.
class Result extends $pb.GeneratedMessage {
  factory Result({
    $core.List<$core.int>? output,
    $core.List<$core.int>? totals,
    $core.List<$core.int>? extremes,
    $core.Iterable<LogEntry>? logs,
    Progress? progress,
    Stats? stats,
    Exception? exception,
    $core.bool? cancelled,
    $core.String? queryId,
    $core.String? timeZone,
    $core.String? outputFormat,
    $core.Iterable<NameAndType>? outputColumns,
  }) {
    final $result = create();
    if (output != null) {
      $result.output = output;
    }
    if (totals != null) {
      $result.totals = totals;
    }
    if (extremes != null) {
      $result.extremes = extremes;
    }
    if (logs != null) {
      $result.logs.addAll(logs);
    }
    if (progress != null) {
      $result.progress = progress;
    }
    if (stats != null) {
      $result.stats = stats;
    }
    if (exception != null) {
      $result.exception = exception;
    }
    if (cancelled != null) {
      $result.cancelled = cancelled;
    }
    if (queryId != null) {
      $result.queryId = queryId;
    }
    if (timeZone != null) {
      $result.timeZone = timeZone;
    }
    if (outputFormat != null) {
      $result.outputFormat = outputFormat;
    }
    if (outputColumns != null) {
      $result.outputColumns.addAll(outputColumns);
    }
    return $result;
  }
  Result._() : super();
  factory Result.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Result.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'Result', package: const $pb.PackageName(_omitMessageNames ? '' : 'clickhouse.grpc'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, _omitFieldNames ? '' : 'output', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, _omitFieldNames ? '' : 'totals', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(3, _omitFieldNames ? '' : 'extremes', $pb.PbFieldType.OY)
    ..pc<LogEntry>(4, _omitFieldNames ? '' : 'logs', $pb.PbFieldType.PM, subBuilder: LogEntry.create)
    ..aOM<Progress>(5, _omitFieldNames ? '' : 'progress', subBuilder: Progress.create)
    ..aOM<Stats>(6, _omitFieldNames ? '' : 'stats', subBuilder: Stats.create)
    ..aOM<Exception>(7, _omitFieldNames ? '' : 'exception', subBuilder: Exception.create)
    ..aOB(8, _omitFieldNames ? '' : 'cancelled')
    ..aOS(9, _omitFieldNames ? '' : 'queryId')
    ..aOS(10, _omitFieldNames ? '' : 'timeZone')
    ..aOS(11, _omitFieldNames ? '' : 'outputFormat')
    ..pc<NameAndType>(12, _omitFieldNames ? '' : 'outputColumns', $pb.PbFieldType.PM, subBuilder: NameAndType.create)
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Result clone() => Result()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Result copyWith(void Function(Result) updates) => super.copyWith((message) => updates(message as Result)) as Result;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static Result create() => Result._();
  Result createEmptyInstance() => create();
  static $pb.PbList<Result> createRepeated() => $pb.PbList<Result>();
  @$core.pragma('dart2js:noInline')
  static Result getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Result>(create);
  static Result? _defaultInstance;

  /// Output of the query, represented in the `output_format`.
  @$pb.TagNumber(1)
  $core.List<$core.int> get output => $_getN(0);
  @$pb.TagNumber(1)
  set output($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOutput() => $_has(0);
  @$pb.TagNumber(1)
  void clearOutput() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get totals => $_getN(1);
  @$pb.TagNumber(2)
  set totals($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTotals() => $_has(1);
  @$pb.TagNumber(2)
  void clearTotals() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get extremes => $_getN(2);
  @$pb.TagNumber(3)
  set extremes($core.List<$core.int> v) { $_setBytes(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasExtremes() => $_has(2);
  @$pb.TagNumber(3)
  void clearExtremes() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<LogEntry> get logs => $_getList(3);

  @$pb.TagNumber(5)
  Progress get progress => $_getN(4);
  @$pb.TagNumber(5)
  set progress(Progress v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasProgress() => $_has(4);
  @$pb.TagNumber(5)
  void clearProgress() => clearField(5);
  @$pb.TagNumber(5)
  Progress ensureProgress() => $_ensure(4);

  @$pb.TagNumber(6)
  Stats get stats => $_getN(5);
  @$pb.TagNumber(6)
  set stats(Stats v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasStats() => $_has(5);
  @$pb.TagNumber(6)
  void clearStats() => clearField(6);
  @$pb.TagNumber(6)
  Stats ensureStats() => $_ensure(5);

  /// Set by the ClickHouse server if there was an exception thrown while executing.
  @$pb.TagNumber(7)
  Exception get exception => $_getN(6);
  @$pb.TagNumber(7)
  set exception(Exception v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasException() => $_has(6);
  @$pb.TagNumber(7)
  void clearException() => clearField(7);
  @$pb.TagNumber(7)
  Exception ensureException() => $_ensure(6);

  /// Set by the ClickHouse server if executing was cancelled by the `cancel` field in QueryInfo.
  @$pb.TagNumber(8)
  $core.bool get cancelled => $_getBF(7);
  @$pb.TagNumber(8)
  set cancelled($core.bool v) { $_setBool(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasCancelled() => $_has(7);
  @$pb.TagNumber(8)
  void clearCancelled() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get queryId => $_getSZ(8);
  @$pb.TagNumber(9)
  set queryId($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasQueryId() => $_has(8);
  @$pb.TagNumber(9)
  void clearQueryId() => clearField(9);

  @$pb.TagNumber(10)
  $core.String get timeZone => $_getSZ(9);
  @$pb.TagNumber(10)
  set timeZone($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasTimeZone() => $_has(9);
  @$pb.TagNumber(10)
  void clearTimeZone() => clearField(10);

  /// The format in which `output`, `totals` and `extremes` are written.
  /// It's either the same as `output_format` specified in `QueryInfo` or the format specified in the query itself.
  @$pb.TagNumber(11)
  $core.String get outputFormat => $_getSZ(10);
  @$pb.TagNumber(11)
  set outputFormat($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasOutputFormat() => $_has(10);
  @$pb.TagNumber(11)
  void clearOutputFormat() => clearField(11);

  /// The names and types of columns of the result written in `output`.
  @$pb.TagNumber(12)
  $core.List<NameAndType> get outputColumns => $_getList(11);
}


const _omitFieldNames = $core.bool.fromEnvironment('protobuf.omit_field_names');
const _omitMessageNames = $core.bool.fromEnvironment('protobuf.omit_message_names');
