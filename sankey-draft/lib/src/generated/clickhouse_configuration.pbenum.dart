//
//  Generated code. Do not modify.
//  source: clickhouse_configuration.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class LogsLevel extends $pb.ProtobufEnum {
  static const LogsLevel LOG_NONE = LogsLevel._(0, _omitEnumNames ? '' : 'LOG_NONE');
  static const LogsLevel LOG_FATAL = LogsLevel._(1, _omitEnumNames ? '' : 'LOG_FATAL');
  static const LogsLevel LOG_CRITICAL = LogsLevel._(2, _omitEnumNames ? '' : 'LOG_CRITICAL');
  static const LogsLevel LOG_ERROR = LogsLevel._(3, _omitEnumNames ? '' : 'LOG_ERROR');
  static const LogsLevel LOG_WARNING = LogsLevel._(4, _omitEnumNames ? '' : 'LOG_WARNING');
  static const LogsLevel LOG_NOTICE = LogsLevel._(5, _omitEnumNames ? '' : 'LOG_NOTICE');
  static const LogsLevel LOG_INFORMATION = LogsLevel._(6, _omitEnumNames ? '' : 'LOG_INFORMATION');
  static const LogsLevel LOG_DEBUG = LogsLevel._(7, _omitEnumNames ? '' : 'LOG_DEBUG');
  static const LogsLevel LOG_TRACE = LogsLevel._(8, _omitEnumNames ? '' : 'LOG_TRACE');

  static const $core.List<LogsLevel> values = <LogsLevel> [
    LOG_NONE,
    LOG_FATAL,
    LOG_CRITICAL,
    LOG_ERROR,
    LOG_WARNING,
    LOG_NOTICE,
    LOG_INFORMATION,
    LOG_DEBUG,
    LOG_TRACE,
  ];

  static final $core.Map<$core.int, LogsLevel> _byValue = $pb.ProtobufEnum.initByValue(values);
  static LogsLevel? valueOf($core.int value) => _byValue[value];

  const LogsLevel._($core.int v, $core.String n) : super(v, n);
}

class ObsoleteTransportCompression_CompressionAlgorithm extends $pb.ProtobufEnum {
  static const ObsoleteTransportCompression_CompressionAlgorithm NO_COMPRESSION = ObsoleteTransportCompression_CompressionAlgorithm._(0, _omitEnumNames ? '' : 'NO_COMPRESSION');
  static const ObsoleteTransportCompression_CompressionAlgorithm DEFLATE = ObsoleteTransportCompression_CompressionAlgorithm._(1, _omitEnumNames ? '' : 'DEFLATE');
  static const ObsoleteTransportCompression_CompressionAlgorithm GZIP = ObsoleteTransportCompression_CompressionAlgorithm._(2, _omitEnumNames ? '' : 'GZIP');
  static const ObsoleteTransportCompression_CompressionAlgorithm STREAM_GZIP = ObsoleteTransportCompression_CompressionAlgorithm._(3, _omitEnumNames ? '' : 'STREAM_GZIP');

  static const $core.List<ObsoleteTransportCompression_CompressionAlgorithm> values = <ObsoleteTransportCompression_CompressionAlgorithm> [
    NO_COMPRESSION,
    DEFLATE,
    GZIP,
    STREAM_GZIP,
  ];

  static final $core.Map<$core.int, ObsoleteTransportCompression_CompressionAlgorithm> _byValue = $pb.ProtobufEnum.initByValue(values);
  static ObsoleteTransportCompression_CompressionAlgorithm? valueOf($core.int value) => _byValue[value];

  const ObsoleteTransportCompression_CompressionAlgorithm._($core.int v, $core.String n) : super(v, n);
}

class ObsoleteTransportCompression_CompressionLevel extends $pb.ProtobufEnum {
  static const ObsoleteTransportCompression_CompressionLevel COMPRESSION_NONE = ObsoleteTransportCompression_CompressionLevel._(0, _omitEnumNames ? '' : 'COMPRESSION_NONE');
  static const ObsoleteTransportCompression_CompressionLevel COMPRESSION_LOW = ObsoleteTransportCompression_CompressionLevel._(1, _omitEnumNames ? '' : 'COMPRESSION_LOW');
  static const ObsoleteTransportCompression_CompressionLevel COMPRESSION_MEDIUM = ObsoleteTransportCompression_CompressionLevel._(2, _omitEnumNames ? '' : 'COMPRESSION_MEDIUM');
  static const ObsoleteTransportCompression_CompressionLevel COMPRESSION_HIGH = ObsoleteTransportCompression_CompressionLevel._(3, _omitEnumNames ? '' : 'COMPRESSION_HIGH');

  static const $core.List<ObsoleteTransportCompression_CompressionLevel> values = <ObsoleteTransportCompression_CompressionLevel> [
    COMPRESSION_NONE,
    COMPRESSION_LOW,
    COMPRESSION_MEDIUM,
    COMPRESSION_HIGH,
  ];

  static final $core.Map<$core.int, ObsoleteTransportCompression_CompressionLevel> _byValue = $pb.ProtobufEnum.initByValue(values);
  static ObsoleteTransportCompression_CompressionLevel? valueOf($core.int value) => _byValue[value];

  const ObsoleteTransportCompression_CompressionLevel._($core.int v, $core.String n) : super(v, n);
}


const _omitEnumNames = $core.bool.fromEnvironment('protobuf.omit_enum_names');
