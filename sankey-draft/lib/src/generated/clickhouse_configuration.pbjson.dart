//
//  Generated code. Do not modify.
//  source: clickhouse_configuration.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use logsLevelDescriptor instead')
const LogsLevel$json = {
  '1': 'LogsLevel',
  '2': [
    {'1': 'LOG_NONE', '2': 0},
    {'1': 'LOG_FATAL', '2': 1},
    {'1': 'LOG_CRITICAL', '2': 2},
    {'1': 'LOG_ERROR', '2': 3},
    {'1': 'LOG_WARNING', '2': 4},
    {'1': 'LOG_NOTICE', '2': 5},
    {'1': 'LOG_INFORMATION', '2': 6},
    {'1': 'LOG_DEBUG', '2': 7},
    {'1': 'LOG_TRACE', '2': 8},
  ],
};

/// Descriptor for `LogsLevel`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List logsLevelDescriptor = $convert.base64Decode(
    'CglMb2dzTGV2ZWwSDAoITE9HX05PTkUQABINCglMT0dfRkFUQUwQARIQCgxMT0dfQ1JJVElDQU'
    'wQAhINCglMT0dfRVJST1IQAxIPCgtMT0dfV0FSTklORxAEEg4KCkxPR19OT1RJQ0UQBRITCg9M'
    'T0dfSU5GT1JNQVRJT04QBhINCglMT0dfREVCVUcQBxINCglMT0dfVFJBQ0UQCA==');

@$core.Deprecated('Use nameAndTypeDescriptor instead')
const NameAndType$json = {
  '1': 'NameAndType',
  '2': [
    {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    {'1': 'type', '3': 2, '4': 1, '5': 9, '10': 'type'},
  ],
};

/// Descriptor for `NameAndType`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nameAndTypeDescriptor = $convert.base64Decode(
    'CgtOYW1lQW5kVHlwZRISCgRuYW1lGAEgASgJUgRuYW1lEhIKBHR5cGUYAiABKAlSBHR5cGU=');

@$core.Deprecated('Use externalTableDescriptor instead')
const ExternalTable$json = {
  '1': 'ExternalTable',
  '2': [
    {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    {'1': 'columns', '3': 2, '4': 3, '5': 11, '6': '.clickhouse.grpc.NameAndType', '10': 'columns'},
    {'1': 'data', '3': 3, '4': 1, '5': 12, '10': 'data'},
    {'1': 'format', '3': 4, '4': 1, '5': 9, '10': 'format'},
    {'1': 'compression_type', '3': 6, '4': 1, '5': 9, '10': 'compressionType'},
    {'1': 'settings', '3': 5, '4': 3, '5': 11, '6': '.clickhouse.grpc.ExternalTable.SettingsEntry', '10': 'settings'},
  ],
  '3': [ExternalTable_SettingsEntry$json],
};

@$core.Deprecated('Use externalTableDescriptor instead')
const ExternalTable_SettingsEntry$json = {
  '1': 'SettingsEntry',
  '2': [
    {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
  '7': {'7': true},
};

/// Descriptor for `ExternalTable`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List externalTableDescriptor = $convert.base64Decode(
    'Cg1FeHRlcm5hbFRhYmxlEhIKBG5hbWUYASABKAlSBG5hbWUSNgoHY29sdW1ucxgCIAMoCzIcLm'
    'NsaWNraG91c2UuZ3JwYy5OYW1lQW5kVHlwZVIHY29sdW1ucxISCgRkYXRhGAMgASgMUgRkYXRh'
    'EhYKBmZvcm1hdBgEIAEoCVIGZm9ybWF0EikKEGNvbXByZXNzaW9uX3R5cGUYBiABKAlSD2NvbX'
    'ByZXNzaW9uVHlwZRJICghzZXR0aW5ncxgFIAMoCzIsLmNsaWNraG91c2UuZ3JwYy5FeHRlcm5h'
    'bFRhYmxlLlNldHRpbmdzRW50cnlSCHNldHRpbmdzGjsKDVNldHRpbmdzRW50cnkSEAoDa2V5GA'
    'EgASgJUgNrZXkSFAoFdmFsdWUYAiABKAlSBXZhbHVlOgI4AQ==');

@$core.Deprecated('Use obsoleteTransportCompressionDescriptor instead')
const ObsoleteTransportCompression$json = {
  '1': 'ObsoleteTransportCompression',
  '2': [
    {'1': 'algorithm', '3': 1, '4': 1, '5': 14, '6': '.clickhouse.grpc.ObsoleteTransportCompression.CompressionAlgorithm', '10': 'algorithm'},
    {'1': 'level', '3': 2, '4': 1, '5': 14, '6': '.clickhouse.grpc.ObsoleteTransportCompression.CompressionLevel', '10': 'level'},
  ],
  '4': [ObsoleteTransportCompression_CompressionAlgorithm$json, ObsoleteTransportCompression_CompressionLevel$json],
};

@$core.Deprecated('Use obsoleteTransportCompressionDescriptor instead')
const ObsoleteTransportCompression_CompressionAlgorithm$json = {
  '1': 'CompressionAlgorithm',
  '2': [
    {'1': 'NO_COMPRESSION', '2': 0},
    {'1': 'DEFLATE', '2': 1},
    {'1': 'GZIP', '2': 2},
    {'1': 'STREAM_GZIP', '2': 3},
  ],
};

@$core.Deprecated('Use obsoleteTransportCompressionDescriptor instead')
const ObsoleteTransportCompression_CompressionLevel$json = {
  '1': 'CompressionLevel',
  '2': [
    {'1': 'COMPRESSION_NONE', '2': 0},
    {'1': 'COMPRESSION_LOW', '2': 1},
    {'1': 'COMPRESSION_MEDIUM', '2': 2},
    {'1': 'COMPRESSION_HIGH', '2': 3},
  ],
};

/// Descriptor for `ObsoleteTransportCompression`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List obsoleteTransportCompressionDescriptor = $convert.base64Decode(
    'ChxPYnNvbGV0ZVRyYW5zcG9ydENvbXByZXNzaW9uEmAKCWFsZ29yaXRobRgBIAEoDjJCLmNsaW'
    'NraG91c2UuZ3JwYy5PYnNvbGV0ZVRyYW5zcG9ydENvbXByZXNzaW9uLkNvbXByZXNzaW9uQWxn'
    'b3JpdGhtUglhbGdvcml0aG0SVAoFbGV2ZWwYAiABKA4yPi5jbGlja2hvdXNlLmdycGMuT2Jzb2'
    'xldGVUcmFuc3BvcnRDb21wcmVzc2lvbi5Db21wcmVzc2lvbkxldmVsUgVsZXZlbCJSChRDb21w'
    'cmVzc2lvbkFsZ29yaXRobRISCg5OT19DT01QUkVTU0lPThAAEgsKB0RFRkxBVEUQARIICgRHWk'
    'lQEAISDwoLU1RSRUFNX0daSVAQAyJrChBDb21wcmVzc2lvbkxldmVsEhQKEENPTVBSRVNTSU9O'
    'X05PTkUQABITCg9DT01QUkVTU0lPTl9MT1cQARIWChJDT01QUkVTU0lPTl9NRURJVU0QAhIUCh'
    'BDT01QUkVTU0lPTl9ISUdIEAM=');

@$core.Deprecated('Use queryInfoDescriptor instead')
const QueryInfo$json = {
  '1': 'QueryInfo',
  '2': [
    {'1': 'query', '3': 1, '4': 1, '5': 9, '10': 'query'},
    {'1': 'query_id', '3': 2, '4': 1, '5': 9, '10': 'queryId'},
    {'1': 'settings', '3': 3, '4': 3, '5': 11, '6': '.clickhouse.grpc.QueryInfo.SettingsEntry', '10': 'settings'},
    {'1': 'database', '3': 4, '4': 1, '5': 9, '10': 'database'},
    {'1': 'input_data', '3': 5, '4': 1, '5': 12, '10': 'inputData'},
    {'1': 'input_data_delimiter', '3': 6, '4': 1, '5': 12, '10': 'inputDataDelimiter'},
    {'1': 'output_format', '3': 7, '4': 1, '5': 9, '10': 'outputFormat'},
    {'1': 'send_output_columns', '3': 24, '4': 1, '5': 8, '10': 'sendOutputColumns'},
    {'1': 'external_tables', '3': 8, '4': 3, '5': 11, '6': '.clickhouse.grpc.ExternalTable', '10': 'externalTables'},
    {'1': 'user_name', '3': 9, '4': 1, '5': 9, '10': 'userName'},
    {'1': 'password', '3': 10, '4': 1, '5': 9, '10': 'password'},
    {'1': 'quota', '3': 11, '4': 1, '5': 9, '10': 'quota'},
    {'1': 'session_id', '3': 12, '4': 1, '5': 9, '10': 'sessionId'},
    {'1': 'session_check', '3': 13, '4': 1, '5': 8, '10': 'sessionCheck'},
    {'1': 'session_timeout', '3': 14, '4': 1, '5': 13, '10': 'sessionTimeout'},
    {'1': 'cancel', '3': 15, '4': 1, '5': 8, '10': 'cancel'},
    {'1': 'next_query_info', '3': 16, '4': 1, '5': 8, '10': 'nextQueryInfo'},
    {'1': 'input_compression_type', '3': 20, '4': 1, '5': 9, '10': 'inputCompressionType'},
    {'1': 'output_compression_type', '3': 21, '4': 1, '5': 9, '10': 'outputCompressionType'},
    {'1': 'output_compression_level', '3': 19, '4': 1, '5': 5, '10': 'outputCompressionLevel'},
    {'1': 'transport_compression_type', '3': 22, '4': 1, '5': 9, '10': 'transportCompressionType'},
    {'1': 'transport_compression_level', '3': 23, '4': 1, '5': 5, '10': 'transportCompressionLevel'},
    {'1': 'obsolete_result_compression', '3': 17, '4': 1, '5': 11, '6': '.clickhouse.grpc.ObsoleteTransportCompression', '10': 'obsoleteResultCompression'},
    {'1': 'obsolete_compression_type', '3': 18, '4': 1, '5': 9, '10': 'obsoleteCompressionType'},
  ],
  '3': [QueryInfo_SettingsEntry$json],
};

@$core.Deprecated('Use queryInfoDescriptor instead')
const QueryInfo_SettingsEntry$json = {
  '1': 'SettingsEntry',
  '2': [
    {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
  '7': {'7': true},
};

/// Descriptor for `QueryInfo`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List queryInfoDescriptor = $convert.base64Decode(
    'CglRdWVyeUluZm8SFAoFcXVlcnkYASABKAlSBXF1ZXJ5EhkKCHF1ZXJ5X2lkGAIgASgJUgdxdW'
    'VyeUlkEkQKCHNldHRpbmdzGAMgAygLMiguY2xpY2tob3VzZS5ncnBjLlF1ZXJ5SW5mby5TZXR0'
    'aW5nc0VudHJ5UghzZXR0aW5ncxIaCghkYXRhYmFzZRgEIAEoCVIIZGF0YWJhc2USHQoKaW5wdX'
    'RfZGF0YRgFIAEoDFIJaW5wdXREYXRhEjAKFGlucHV0X2RhdGFfZGVsaW1pdGVyGAYgASgMUhJp'
    'bnB1dERhdGFEZWxpbWl0ZXISIwoNb3V0cHV0X2Zvcm1hdBgHIAEoCVIMb3V0cHV0Rm9ybWF0Ei'
    '4KE3NlbmRfb3V0cHV0X2NvbHVtbnMYGCABKAhSEXNlbmRPdXRwdXRDb2x1bW5zEkcKD2V4dGVy'
    'bmFsX3RhYmxlcxgIIAMoCzIeLmNsaWNraG91c2UuZ3JwYy5FeHRlcm5hbFRhYmxlUg5leHRlcm'
    '5hbFRhYmxlcxIbCgl1c2VyX25hbWUYCSABKAlSCHVzZXJOYW1lEhoKCHBhc3N3b3JkGAogASgJ'
    'UghwYXNzd29yZBIUCgVxdW90YRgLIAEoCVIFcXVvdGESHQoKc2Vzc2lvbl9pZBgMIAEoCVIJc2'
    'Vzc2lvbklkEiMKDXNlc3Npb25fY2hlY2sYDSABKAhSDHNlc3Npb25DaGVjaxInCg9zZXNzaW9u'
    'X3RpbWVvdXQYDiABKA1SDnNlc3Npb25UaW1lb3V0EhYKBmNhbmNlbBgPIAEoCFIGY2FuY2VsEi'
    'YKD25leHRfcXVlcnlfaW5mbxgQIAEoCFINbmV4dFF1ZXJ5SW5mbxI0ChZpbnB1dF9jb21wcmVz'
    'c2lvbl90eXBlGBQgASgJUhRpbnB1dENvbXByZXNzaW9uVHlwZRI2ChdvdXRwdXRfY29tcHJlc3'
    'Npb25fdHlwZRgVIAEoCVIVb3V0cHV0Q29tcHJlc3Npb25UeXBlEjgKGG91dHB1dF9jb21wcmVz'
    'c2lvbl9sZXZlbBgTIAEoBVIWb3V0cHV0Q29tcHJlc3Npb25MZXZlbBI8Chp0cmFuc3BvcnRfY2'
    '9tcHJlc3Npb25fdHlwZRgWIAEoCVIYdHJhbnNwb3J0Q29tcHJlc3Npb25UeXBlEj4KG3RyYW5z'
    'cG9ydF9jb21wcmVzc2lvbl9sZXZlbBgXIAEoBVIZdHJhbnNwb3J0Q29tcHJlc3Npb25MZXZlbB'
    'JtChtvYnNvbGV0ZV9yZXN1bHRfY29tcHJlc3Npb24YESABKAsyLS5jbGlja2hvdXNlLmdycGMu'
    'T2Jzb2xldGVUcmFuc3BvcnRDb21wcmVzc2lvblIZb2Jzb2xldGVSZXN1bHRDb21wcmVzc2lvbh'
    'I6ChlvYnNvbGV0ZV9jb21wcmVzc2lvbl90eXBlGBIgASgJUhdvYnNvbGV0ZUNvbXByZXNzaW9u'
    'VHlwZRo7Cg1TZXR0aW5nc0VudHJ5EhAKA2tleRgBIAEoCVIDa2V5EhQKBXZhbHVlGAIgASgJUg'
    'V2YWx1ZToCOAE=');

@$core.Deprecated('Use logEntryDescriptor instead')
const LogEntry$json = {
  '1': 'LogEntry',
  '2': [
    {'1': 'time', '3': 1, '4': 1, '5': 13, '10': 'time'},
    {'1': 'time_microseconds', '3': 2, '4': 1, '5': 13, '10': 'timeMicroseconds'},
    {'1': 'thread_id', '3': 3, '4': 1, '5': 4, '10': 'threadId'},
    {'1': 'query_id', '3': 4, '4': 1, '5': 9, '10': 'queryId'},
    {'1': 'level', '3': 5, '4': 1, '5': 14, '6': '.clickhouse.grpc.LogsLevel', '10': 'level'},
    {'1': 'source', '3': 6, '4': 1, '5': 9, '10': 'source'},
    {'1': 'text', '3': 7, '4': 1, '5': 9, '10': 'text'},
  ],
};

/// Descriptor for `LogEntry`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List logEntryDescriptor = $convert.base64Decode(
    'CghMb2dFbnRyeRISCgR0aW1lGAEgASgNUgR0aW1lEisKEXRpbWVfbWljcm9zZWNvbmRzGAIgAS'
    'gNUhB0aW1lTWljcm9zZWNvbmRzEhsKCXRocmVhZF9pZBgDIAEoBFIIdGhyZWFkSWQSGQoIcXVl'
    'cnlfaWQYBCABKAlSB3F1ZXJ5SWQSMAoFbGV2ZWwYBSABKA4yGi5jbGlja2hvdXNlLmdycGMuTG'
    '9nc0xldmVsUgVsZXZlbBIWCgZzb3VyY2UYBiABKAlSBnNvdXJjZRISCgR0ZXh0GAcgASgJUgR0'
    'ZXh0');

@$core.Deprecated('Use progressDescriptor instead')
const Progress$json = {
  '1': 'Progress',
  '2': [
    {'1': 'read_rows', '3': 1, '4': 1, '5': 4, '10': 'readRows'},
    {'1': 'read_bytes', '3': 2, '4': 1, '5': 4, '10': 'readBytes'},
    {'1': 'total_rows_to_read', '3': 3, '4': 1, '5': 4, '10': 'totalRowsToRead'},
    {'1': 'written_rows', '3': 4, '4': 1, '5': 4, '10': 'writtenRows'},
    {'1': 'written_bytes', '3': 5, '4': 1, '5': 4, '10': 'writtenBytes'},
  ],
};

/// Descriptor for `Progress`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List progressDescriptor = $convert.base64Decode(
    'CghQcm9ncmVzcxIbCglyZWFkX3Jvd3MYASABKARSCHJlYWRSb3dzEh0KCnJlYWRfYnl0ZXMYAi'
    'ABKARSCXJlYWRCeXRlcxIrChJ0b3RhbF9yb3dzX3RvX3JlYWQYAyABKARSD3RvdGFsUm93c1Rv'
    'UmVhZBIhCgx3cml0dGVuX3Jvd3MYBCABKARSC3dyaXR0ZW5Sb3dzEiMKDXdyaXR0ZW5fYnl0ZX'
    'MYBSABKARSDHdyaXR0ZW5CeXRlcw==');

@$core.Deprecated('Use statsDescriptor instead')
const Stats$json = {
  '1': 'Stats',
  '2': [
    {'1': 'rows', '3': 1, '4': 1, '5': 4, '10': 'rows'},
    {'1': 'blocks', '3': 2, '4': 1, '5': 4, '10': 'blocks'},
    {'1': 'allocated_bytes', '3': 3, '4': 1, '5': 4, '10': 'allocatedBytes'},
    {'1': 'applied_limit', '3': 4, '4': 1, '5': 8, '10': 'appliedLimit'},
    {'1': 'rows_before_limit', '3': 5, '4': 1, '5': 4, '10': 'rowsBeforeLimit'},
  ],
};

/// Descriptor for `Stats`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List statsDescriptor = $convert.base64Decode(
    'CgVTdGF0cxISCgRyb3dzGAEgASgEUgRyb3dzEhYKBmJsb2NrcxgCIAEoBFIGYmxvY2tzEicKD2'
    'FsbG9jYXRlZF9ieXRlcxgDIAEoBFIOYWxsb2NhdGVkQnl0ZXMSIwoNYXBwbGllZF9saW1pdBgE'
    'IAEoCFIMYXBwbGllZExpbWl0EioKEXJvd3NfYmVmb3JlX2xpbWl0GAUgASgEUg9yb3dzQmVmb3'
    'JlTGltaXQ=');

@$core.Deprecated('Use exceptionDescriptor instead')
const Exception$json = {
  '1': 'Exception',
  '2': [
    {'1': 'code', '3': 1, '4': 1, '5': 5, '10': 'code'},
    {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    {'1': 'display_text', '3': 3, '4': 1, '5': 9, '10': 'displayText'},
    {'1': 'stack_trace', '3': 4, '4': 1, '5': 9, '10': 'stackTrace'},
  ],
};

/// Descriptor for `Exception`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List exceptionDescriptor = $convert.base64Decode(
    'CglFeGNlcHRpb24SEgoEY29kZRgBIAEoBVIEY29kZRISCgRuYW1lGAIgASgJUgRuYW1lEiEKDG'
    'Rpc3BsYXlfdGV4dBgDIAEoCVILZGlzcGxheVRleHQSHwoLc3RhY2tfdHJhY2UYBCABKAlSCnN0'
    'YWNrVHJhY2U=');

@$core.Deprecated('Use resultDescriptor instead')
const Result$json = {
  '1': 'Result',
  '2': [
    {'1': 'query_id', '3': 9, '4': 1, '5': 9, '10': 'queryId'},
    {'1': 'time_zone', '3': 10, '4': 1, '5': 9, '10': 'timeZone'},
    {'1': 'output_format', '3': 11, '4': 1, '5': 9, '10': 'outputFormat'},
    {'1': 'output_columns', '3': 12, '4': 3, '5': 11, '6': '.clickhouse.grpc.NameAndType', '10': 'outputColumns'},
    {'1': 'output', '3': 1, '4': 1, '5': 12, '10': 'output'},
    {'1': 'totals', '3': 2, '4': 1, '5': 12, '10': 'totals'},
    {'1': 'extremes', '3': 3, '4': 1, '5': 12, '10': 'extremes'},
    {'1': 'logs', '3': 4, '4': 3, '5': 11, '6': '.clickhouse.grpc.LogEntry', '10': 'logs'},
    {'1': 'progress', '3': 5, '4': 1, '5': 11, '6': '.clickhouse.grpc.Progress', '10': 'progress'},
    {'1': 'stats', '3': 6, '4': 1, '5': 11, '6': '.clickhouse.grpc.Stats', '10': 'stats'},
    {'1': 'exception', '3': 7, '4': 1, '5': 11, '6': '.clickhouse.grpc.Exception', '10': 'exception'},
    {'1': 'cancelled', '3': 8, '4': 1, '5': 8, '10': 'cancelled'},
  ],
};

/// Descriptor for `Result`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List resultDescriptor = $convert.base64Decode(
    'CgZSZXN1bHQSGQoIcXVlcnlfaWQYCSABKAlSB3F1ZXJ5SWQSGwoJdGltZV96b25lGAogASgJUg'
    'h0aW1lWm9uZRIjCg1vdXRwdXRfZm9ybWF0GAsgASgJUgxvdXRwdXRGb3JtYXQSQwoOb3V0cHV0'
    'X2NvbHVtbnMYDCADKAsyHC5jbGlja2hvdXNlLmdycGMuTmFtZUFuZFR5cGVSDW91dHB1dENvbH'
    'VtbnMSFgoGb3V0cHV0GAEgASgMUgZvdXRwdXQSFgoGdG90YWxzGAIgASgMUgZ0b3RhbHMSGgoI'
    'ZXh0cmVtZXMYAyABKAxSCGV4dHJlbWVzEi0KBGxvZ3MYBCADKAsyGS5jbGlja2hvdXNlLmdycG'
    'MuTG9nRW50cnlSBGxvZ3MSNQoIcHJvZ3Jlc3MYBSABKAsyGS5jbGlja2hvdXNlLmdycGMuUHJv'
    'Z3Jlc3NSCHByb2dyZXNzEiwKBXN0YXRzGAYgASgLMhYuY2xpY2tob3VzZS5ncnBjLlN0YXRzUg'
    'VzdGF0cxI4CglleGNlcHRpb24YByABKAsyGi5jbGlja2hvdXNlLmdycGMuRXhjZXB0aW9uUgll'
    'eGNlcHRpb24SHAoJY2FuY2VsbGVkGAggASgIUgljYW5jZWxsZWQ=');

