//
//  Generated code. Do not modify.
//  source: clickhouse_configuration.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:async' as $async;
import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'package:protobuf/protobuf.dart' as $pb;

import 'clickhouse_configuration.pb.dart' as $0;

export 'clickhouse_configuration.pb.dart';

@$pb.GrpcServiceName('clickhouse.grpc.ClickHouse')
class ClickHouseClient extends $grpc.Client {
  static final _$executeQuery = $grpc.ClientMethod<$0.QueryInfo, $0.Result>(
      '/clickhouse.grpc.ClickHouse/ExecuteQuery',
      ($0.QueryInfo value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Result.fromBuffer(value));
  static final _$executeQueryWithStreamInput = $grpc.ClientMethod<$0.QueryInfo, $0.Result>(
      '/clickhouse.grpc.ClickHouse/ExecuteQueryWithStreamInput',
      ($0.QueryInfo value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Result.fromBuffer(value));
  static final _$executeQueryWithStreamOutput = $grpc.ClientMethod<$0.QueryInfo, $0.Result>(
      '/clickhouse.grpc.ClickHouse/ExecuteQueryWithStreamOutput',
      ($0.QueryInfo value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Result.fromBuffer(value));
  static final _$executeQueryWithStreamIO = $grpc.ClientMethod<$0.QueryInfo, $0.Result>(
      '/clickhouse.grpc.ClickHouse/ExecuteQueryWithStreamIO',
      ($0.QueryInfo value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Result.fromBuffer(value));

  ClickHouseClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options,
        interceptors: interceptors);

  $grpc.ResponseFuture<$0.Result> executeQuery($0.QueryInfo request, {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$executeQuery, request, options: options);
  }

  $grpc.ResponseFuture<$0.Result> executeQueryWithStreamInput($async.Stream<$0.QueryInfo> request, {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$executeQueryWithStreamInput, request, options: options).single;
  }

  $grpc.ResponseStream<$0.Result> executeQueryWithStreamOutput($0.QueryInfo request, {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$executeQueryWithStreamOutput, $async.Stream.fromIterable([request]), options: options);
  }

  $grpc.ResponseStream<$0.Result> executeQueryWithStreamIO($async.Stream<$0.QueryInfo> request, {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$executeQueryWithStreamIO, request, options: options);
  }
}

@$pb.GrpcServiceName('clickhouse.grpc.ClickHouse')
abstract class ClickHouseServiceBase extends $grpc.Service {
  $core.String get $name => 'clickhouse.grpc.ClickHouse';

  ClickHouseServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.QueryInfo, $0.Result>(
        'ExecuteQuery',
        executeQuery_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.QueryInfo.fromBuffer(value),
        ($0.Result value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.QueryInfo, $0.Result>(
        'ExecuteQueryWithStreamInput',
        executeQueryWithStreamInput,
        true,
        false,
        ($core.List<$core.int> value) => $0.QueryInfo.fromBuffer(value),
        ($0.Result value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.QueryInfo, $0.Result>(
        'ExecuteQueryWithStreamOutput',
        executeQueryWithStreamOutput_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.QueryInfo.fromBuffer(value),
        ($0.Result value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.QueryInfo, $0.Result>(
        'ExecuteQueryWithStreamIO',
        executeQueryWithStreamIO,
        true,
        true,
        ($core.List<$core.int> value) => $0.QueryInfo.fromBuffer(value),
        ($0.Result value) => value.writeToBuffer()));
  }

  $async.Future<$0.Result> executeQuery_Pre($grpc.ServiceCall call, $async.Future<$0.QueryInfo> request) async {
    return executeQuery(call, await request);
  }

  $async.Stream<$0.Result> executeQueryWithStreamOutput_Pre($grpc.ServiceCall call, $async.Future<$0.QueryInfo> request) async* {
    yield* executeQueryWithStreamOutput(call, await request);
  }

  $async.Future<$0.Result> executeQuery($grpc.ServiceCall call, $0.QueryInfo request);
  $async.Future<$0.Result> executeQueryWithStreamInput($grpc.ServiceCall call, $async.Stream<$0.QueryInfo> request);
  $async.Stream<$0.Result> executeQueryWithStreamOutput($grpc.ServiceCall call, $0.QueryInfo request);
  $async.Stream<$0.Result> executeQueryWithStreamIO($grpc.ServiceCall call, $async.Stream<$0.QueryInfo> request);
}
