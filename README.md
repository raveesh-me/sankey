# Sankey
Set of tools to create sankey diagrams from ingested navigation data.
This is built on top of Clickhouse DB.

Consists of:

### Sankey Ingester
A web deployed middleware that ingests logs and writes to the clickhouse database

### Sankey Visualiser
A flutter app that should be able to connect to a clickhouse databse and visualise the sankey diagram

### Sankey Watcher
Import relevant plugin to your project and inject it as a watcher on the navigator. 
* For navigation events, watcher will log event automatically
* For logging actions while remaining on the same page, use the global provided by the platform plugin

#### Sankey Listener

#### Sankey Router Inject


<!-- Brainstorming and current experiment -->

If we create a single backend to:
1. Receive data as "Flow", or a "List of Flows"
2. Send data back as a "Consolidated Flow" which will be the rendering unit

A flow is a list of interactions. The model will be as:

```json
Flow{
    qualifier: "version_x_wl_company_id_x_whatever", //flows will be consolidated and filtered with qualifiers
    consolidation_time: DATE_TIME,
    interactions: [... Interaction{
        index: 0-x,
        type: action | navigation | custom | abandon | init,
        from: '/',
        to: '/messages/{conversationID}',
    }]
}
```

The backend will keep consolidating the flow per qualifier. The model of the database will be to the extent of:
```json
{
    "qualifier"  : ConsolidatedFlowData{
        interactions: [{
            index: 0-x
            from: "",
            to: "",
            weight: 1800,


        }]
    }
}
```
This way, the frontend can either fetch all consolidated data, or data for a certain qualfier and render it immediately. No render compute required.